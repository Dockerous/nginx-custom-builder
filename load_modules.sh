declare -A LOAD_REGULAR_MODULES
declare -A LOAD_DYNAMIC_MODULES
declare -A LOAD_DYNAMIC_SOURCE
declare -A DEFAULT_DYNAMIC_MODULES
declare -A DEFAULT_DYNAMIC_SOURCE
declare -A ADD_CONFIGURE_PARAMS=" "

##########
# 
# Add NGINX parameters complete list of available parameters can be found at
# https://github.com/nginx/nginx/blob/master/auto/options
#
# Links to documentation are provided when possible
# https://www.nginx.com/resources/admin-guide/installing-nginx-open-source/#configure
#
##########

ADD_CONFIGURE_PARAMS+="--with-compat "
ADD_CONFIGURE_PARAMS+="--with-debug "
ADD_CONFIGURE_PARAMS+="--with-threads "
ADD_CONFIGURE_PARAMS+="--with-file-aio "
ADD_CONFIGURE_PARAMS+="--with-cpp_test_module "
#ADD_CONFIGURE_PARAMS+="--with-libatomic " - currently produces error
ADD_CONFIGURE_PARAMS+="--with-pcre-jit "

##########
# 
# Used to Disable default NGINX modules
# https://nginx.org/en/docs/configure.html
#
##########
#ADD_CONFIGURE_PARAMS+="--without-select_module "
#ADD_CONFIGURE_PARAMS+="--without-poll_module "
#ADD_CONFIGURE_PARAMS+="--without-http_gzip_module "
#ADD_CONFIGURE_PARAMS+="--without-http_rewrite_module "
#ADD_CONFIGURE_PARAMS+="--without-http_proxy_module "

##########
# 
# Add NGINX Modules complete list can be found at
# https://nginx.org/en/docs/
#
##########
ADD_CONFIGURE_PARAMS+="--with-http_addition_module "
ADD_CONFIGURE_PARAMS+="--with-http_auth_request_module "
ADD_CONFIGURE_PARAMS+="--with-http_dav_module "
ADD_CONFIGURE_PARAMS+="--with-http_flv_module "
ADD_CONFIGURE_PARAMS+="--with-http_gunzip_module "
ADD_CONFIGURE_PARAMS+="--with-http_gzip_static_module "
ADD_CONFIGURE_PARAMS+="--with-http_mp4_module "
ADD_CONFIGURE_PARAMS+="--with-http_random_index_module "
ADD_CONFIGURE_PARAMS+="--with-http_realip_module "
ADD_CONFIGURE_PARAMS+="--with-http_secure_link_module "
ADD_CONFIGURE_PARAMS+="--with-http_slice_module "
ADD_CONFIGURE_PARAMS+="--with-http_ssl_module "
ADD_CONFIGURE_PARAMS+="--with-http_stub_status_module "
ADD_CONFIGURE_PARAMS+="--with-http_sub_module "
ADD_CONFIGURE_PARAMS+="--with-http_v2_module "
ADD_CONFIGURE_PARAMS+="--with-mail_ssl_module "
ADD_CONFIGURE_PARAMS+="--with-stream_realip_module "
ADD_CONFIGURE_PARAMS+="--with-stream_ssl_module "
ADD_CONFIGURE_PARAMS+="--with-stream_ssl_preread_module "
ADD_CONFIGURE_PARAMS+="--with-google_perftools_module "

###
#  Following Modules have been moved to the
#  dynamic module list below
###
#ADD_CONFIGURE_PARAMS+="--with-http_geoip_module "
#ADD_CONFIGURE_PARAMS+="--with-http_image_filter_module "
#ADD_CONFIGURE_PARAMS+="--with-mail "
#ADD_CONFIGURE_PARAMS+="--with-stream "
#ADD_CONFIGURE_PARAMS+="--with-http_xslt_module "
#ADD_CONFIGURE_PARAMS+="--with-http_perl_module "
#ADD_CONFIGURE_PARAMS+="--with-stream_geoip_module "


#######
#
#  Uncomment any module you wish to load
#
#######

 
###

#LOAD_REGULAR_MODULES['ngx_drizzle']="https://github.com/openresty/drizzle-nginx-module/zipball/master" #- an nginx upstream module that talks to mysql and drizzle by libdrizzle. 
#LOAD_REGULAR_MODULES['nginx-fancyindex']="https://github.com/damm/nginx-fancyindex" #- nginx fancy index module. 
LOAD_REGULAR_MODULES['ngx_http_footer_filter']="https://github.com/alibaba/nginx-http-footer-filter/zipball/master" #- A nginx module that prints some text in the footer of a request.

# Load Balancers
#LOAD_REGULAR_MODULES['ngx_upstream_fair']="https://github.com/gnosek/nginx-upstream-fair/zipball/master" #- The fair load balancer module for nginx.

LOAD_REGULAR_MODULES['ngx_url']="https://github.com/vozlt/nginx-module-url/zipball/master" #- Nginx url encoding converting module.
LOAD_REGULAR_MODULES['ngx_protobuf_nginx']="https://github.com/dbcode/protobuf-nginx/zipball/master" #- Google Protocol Buffers code generator for nginx module developers. 
LOAD_REGULAR_MODULES['ngx_http_substitutions_filter_module']="https://github.com/yaoweibin/ngx_http_substitutions_filter_module" #- a filter module which can do both regular expression and fixed string substitutions for nginx.

#######
#  Dynamic Modules
#  Set value to:
#     no = adds dynamic module but doesn't add to nginx.conf
#     yes = adds dynamic module and adds it to nginx.conf
#     regular = adds the module like a regular module
#######
DEFAULT_DYNAMIC_MODULES['--with-http_geoip_module']='no'
DEFAULT_DYNAMIC_MODULES['--with-http_image_filter_module']='no'
DEFAULT_DYNAMIC_MODULES['--with-mail']='yes'
DEFAULT_DYNAMIC_MODULES['--with-stream']='no'
DEFAULT_DYNAMIC_MODULES['--with-http_xslt_module']='no'
#DEFAULT_DYNAMIC_MODULES['--with-http_perl_module']='yes' - currently produces error
DEFAULT_DYNAMIC_MODULES['--with-stream_geoip_module']='no'

LOAD_DYNAMIC_MODULES['ngx_pagespeed']='yes' #"https://github.com/pagespeed/ngx_pagespeed/zipball/master" #- Automatic PageSpeed optimization module for Nginx. 
LOAD_DYNAMIC_MODULES['ngx_srcache']='yes' #"https://github.com/openresty/srcache-nginx-module/zipball/master" #- Transparent subrequest-based caching layout for arbitrary nginx locations. 
LOAD_DYNAMIC_MODULES['ngx_echo']='yes' #"https://github.com/openresty/echo-nginx-module/zipball/master" #- An Nginx module for bringing the power of "echo", "sleep", "time" and more to Nginx's config file. 
LOAD_DYNAMIC_MODULES['ngx_headers_more']='yes' #"https://github.com/openresty/headers-more-nginx-module/zipball/master" #- Set, add, and clear arbitrary output headers. 

# Databases and Connectors
LOAD_DYNAMIC_MODULES['ngx_redis2']='yes' #"https://github.com/openresty/redis2-nginx-module/zipball/master" #- Nginx upstream module for the Redis 2.0 protocol. 

# Transformers
LOAD_DYNAMIC_MODULES['ngx_small_light']='no' #"https://github.com/cubicdaiya/ngx_small_light/zipball/master" #- Dynamic Image Transformation Module For nginx. 
LOAD_DYNAMIC_MODULES['ngx_upload_progress_module']='yes' #"https://github.com/masterzen/nginx-upload-progress-module/zipball/master" #- Nginx module implementing an upload progress system, that monitors RFC1867 POST uploads as they are transmitted to upstream servers. 

###Lua Modules
# Databases and Connectors in Lua
LOAD_DYNAMIC_MODULES['ngx_lua']='no' #"https://github.com/openresty/lua-nginx-module/zipball/master"



DEFAULT_DYNAMIC_SOURCE['--with-http_geoip_module']='ngx_http_geoip_module.so'
DEFAULT_DYNAMIC_SOURCE['--with-http_image_filter_module']='ngx_http_image_filter_module.so'
DEFAULT_DYNAMIC_SOURCE['--with-mail']='ngx_mail_module.so'
DEFAULT_DYNAMIC_SOURCE['--with-stream']='ngx_stream_module.so'
DEFAULT_DYNAMIC_SOURCE['--with-http_xslt_module']='ngx_http_xslt_filter_module.so'
#DEFAULT_DYNAMIC_SOURCE['--with-http_perl_module']='yes' - currently produces error
DEFAULT_DYNAMIC_SOURCE['--with-stream_geoip_module']='ngx_stream_geoip_module.so'

LOAD_DYNAMIC_SOURCE['ngx_pagespeed']='ngx_pagespeed.so'
LOAD_DYNAMIC_SOURCE['ngx_srcache']='ngx_http_srcache_filter_module.so'
LOAD_DYNAMIC_SOURCE['ngx_echo']='ngx_http_echo_module.so'
LOAD_DYNAMIC_SOURCE['ngx_headers_more']='ngx_http_headers_more_filter_module.so' 

# Databases and Connectors
LOAD_DYNAMIC_SOURCE['ngx_redis2']='ngx_http_redis2_module.so'

# Transformers
LOAD_DYNAMIC_SOURCE['ngx_small_light']='ngx_http_small_light_module.so'
LOAD_DYNAMIC_SOURCE['ngx_upload_progress_module']='ngx_http_uploadprogress_module.so'

###Lua Modules
# Databases and Connectors in Lua
LOAD_DYNAMIC_SOURCE['ngx_lua']='ngx_http_lua_module.so'
