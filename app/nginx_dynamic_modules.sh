declare -A NGINX_DYNAMIC_MODULES # where do I get the zip?
# some modules require extra custome work. 
# store a .sh in module_deps and declare it here with the same module name
# ex: ngx_brotli --> app/module_deps/ngx_brotli-install.sh (-install, -run, -update, -remove)

###

NGINX_DYNAMIC_MODULES['ngx_pagespeed']="https://github.com/pagespeed/ngx_pagespeed/zipball/master" #- Automatic PageSpeed optimization module for Nginx. 
NGINX_DYNAMIC_MODULES['ngx_srcache']="https://github.com/openresty/srcache-nginx-module/zipball/master" #- Transparent subrequest-based caching layout for arbitrary nginx locations. 
NGINX_DYNAMIC_MODULES['ngx_echo']="https://github.com/openresty/echo-nginx-module/zipball/master" #- An Nginx module for bringing the power of "echo", "sleep", "time" and more to Nginx's config file. 
NGINX_DYNAMIC_MODULES['ngx_headers_more']="https://github.com/openresty/headers-more-nginx-module/zipball/master" #- Set, add, and clear arbitrary output headers. 

# Databases and Connectors
NGINX_DYNAMIC_MODULES['ngx_redis2']="https://github.com/openresty/redis2-nginx-module/zipball/master" #- Nginx upstream module for the Redis 2.0 protocol. 

# Transformers
NGINX_DYNAMIC_MODULES['ngx_small_light']="https://github.com/cubicdaiya/ngx_small_light/zipball/master" #- Dynamic Image Transformation Module For nginx. 

NGINX_DYNAMIC_MODULES['ngx_upload_progress_module']="https://github.com/masterzen/nginx-upload-progress-module/zipball/master" #- Nginx module implementing an upload progress system, that monitors RFC1867 POST uploads as they are transmitted to upstream servers. 

###Lua Modules
# Databases and Connectors in Lua
NGINX_DYNAMIC_MODULES['ngx_lua']="https://github.com/openresty/lua-nginx-module/zipball/master"

