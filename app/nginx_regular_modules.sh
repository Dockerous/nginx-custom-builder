declare -A NGINX_REGULAR_MODULES # where do I get the zip?
# some modules require extra custome work. 
# store a .sh in module_deps and declare it here with the same module name
# ex: ngx_brotli --> app/module_deps/ngx_brotli-install.sh (-install, -run, -update, -remove)

###

NGINX_REGULAR_MODULES['ngx_drizzle']="https://github.com/openresty/drizzle-nginx-module/zipball/master" #- an nginx upstream module that talks to mysql and drizzle by libdrizzle. 
NGINX_REGULAR_MODULES['nginx-fancyindex']="https://github.com/damm/nginx-fancyindex" #- nginx fancy index module. 
NGINX_REGULAR_MODULES['ngx_http_footer_filter']="https://github.com/alibaba/nginx-http-footer-filter/zipball/master" #- A nginx module that prints some text in the footer of a request.

# Load Balancers
NGINX_REGULAR_MODULES['ngx_upstream_fair']="https://github.com/gnosek/nginx-upstream-fair/zipball/master" #- The fair load balancer module for nginx.

NGINX_REGULAR_MODULES['ngx_url']="https://github.com/vozlt/nginx-module-url/zipball/master" #- Nginx url encoding converting module.
NGINX_REGULAR_MODULES['ngx_protobuf_nginx']="https://github.com/dbcode/protobuf-nginx/zipball/master" #- Google Protocol Buffers code generator for nginx module developers. 
NGINX_REGULAR_MODULES['ngx_http_substitutions_filter_module']="https://github.com/yaoweibin/ngx_http_substitutions_filter_module" #- a filter module which can do both regular expression and fixed string substitutions for nginx.
